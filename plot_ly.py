from coord import check_data, numpy, Delaunay
from prmpsmart_plotly_min_js import PRMPSMART_STARTING_TAG, PRMPSMART_ENDING_TAG
from plotly import figure_factory, graph_objects, colors


class PRMP_Plotly_Plotter3D:
    PLOTLY_SCALES = list(colors.PLOTLY_SCALES.keys())

    def __init__(self, file):
        self.default_kwargs = dict(
            template="plotly",
            x_axis_ticks=0,
            y_axis_ticks=0,
            z_axis_ticks=0,
            x_aspect_ratio=1,
            y_aspect_ratio=1,
            z_aspect_ratio=0.5,
            x_contour=False,
            y_contour=False,
            z_contour=False,
            colorscale="plotly3",
            color="white",
            width=2,
        )
        self.file = file
        self.kwargs = {}

        self.x: numpy.ndarray = None
        self.y: numpy.ndarray = None
        self.z: numpy.ndarray = None

        self.set_default_kwargs()

    def fit_data(self, x=None, y=None, z=None):
        self.x = x
        self.y = y
        self.z = z

    def check_data(self):
        return check_data(self.x) == check_data(self.y) == check_data(self.z)

    def set_default_kwargs(self):
        self.kwargs = self.default_kwargs.copy()

    @property
    def colorscale(self):
        return self.kwargs.get("colorscale")

    @property
    def color(self):
        return self.kwargs.get("color") or "white"

    @property
    def width(self):
        return self.kwargs.get("width")

    def set_parameters(self, kwargs):
        self.kwargs.update(**kwargs)

    def get_traces(self):
        traces = {}
        x_contour = self.kwargs.get("x_contour")
        y_contour = self.kwargs.get("y_contour")
        z_contour = self.kwargs.get("z_contour")
        if x_contour:
            traces.update(
                contours_x=dict(
                    show=True,
                    usecolormap=True,
                    project_x=True,
                )
            )
        if y_contour:
            traces.update(
                contours_y=dict(
                    show=True,
                    usecolormap=True,
                    project_y=True,
                )
            )
        if z_contour:
            traces.update(
                contours_z=dict(
                    show=True,
                    usecolormap=True,
                    project_z=True,
                )
            )
        return traces

    def get_layout(self, aspectratio=False):
        template = self.kwargs.get("template", self.default_kwargs.get("template"))
        x_axis_ticks = self.kwargs.get(
            "x_axis_ticks", self.default_kwargs.get("x_axis_ticks")
        )
        y_axis_ticks = self.kwargs.get(
            "y_axis_ticks", self.default_kwargs.get("y_axis_ticks")
        )
        z_axis_ticks = self.kwargs.get(
            "z_axis_ticks", self.default_kwargs.get("z_axis_ticks")
        )
        x_aspect_ratio = self.kwargs.get(
            "x_aspect_ratio", self.default_kwargs.get("x_aspect_ratio")
        )
        y_aspect_ratio = self.kwargs.get(
            "y_aspect_ratio", self.default_kwargs.get("y_aspect_ratio")
        )
        z_aspect_ratio = self.kwargs.get(
            "z_aspect_ratio", self.default_kwargs.get("z_aspect_ratio")
        )

        x_ticks = dict(tickformat="digit")
        y_ticks = x_ticks.copy()
        scene = dict(
            aspectratio=dict(x=x_aspect_ratio, y=y_aspect_ratio, z=z_aspect_ratio),
            xaxis=x_ticks,
            yaxis=y_ticks,
        )

        if aspectratio:
            return scene

        if x_axis_ticks:
            x_ticks.update(
                nticks=x_axis_ticks,
            )

        if y_axis_ticks:
            y_ticks.update(
                nticks=y_axis_ticks,
            )

        if z_axis_ticks:
            scene.update(
                zaxis=dict(
                    nticks=z_axis_ticks,
                )
            )

        layout = dict(
            title="",
            autosize=True,
            scene=scene,
            template=template,
            margin=dict(l=10, t=20, b=10, r=10),
        )

        return layout

    def write_plot(self, fig):
        div = fig.to_html(include_plotlyjs=False, full_html=False, validate=False)
        html = PRMPSMART_STARTING_TAG + div + PRMPSMART_ENDING_TAG
        open(self.file, "w").write(html)
        return self.file

    def surface_3d(self):
        # x, y are one array
        # z is a grid
        kwargs = dict(
            x=self.x,
            y=self.y,
            z=self.z,
            customdata=numpy.stack((self.x, self.y, self.z), axis=-1),
            hovertemplate="Lon: %{customdata[0]: d}<br>Lat: %{customdata[1]: d}<br>Topo: %{customdata[2]: .2f}<extra></extra>",
        )

        if self.colorscale:
            kwargs["colorscale"] = self.colorscale

        surf = graph_objects.Surface(**kwargs)
        return surf, None

    def wireframe_3d(self):
        # x, y, z are grids
        lines = []
        line = dict(color=self.color, width=self.width)
        for x, y, z in zip(self.x, self.y, self.z):
            scatter = graph_objects.Scatter3d(x=x, y=y, z=z, mode="lines", line=line)
            lines.append(scatter)
        layout = graph_objects.Layout(
            title="Wireframe Plot",
            scene=dict(
                xaxis=dict(
                    gridcolor="rgb(255, 255, 255)",
                    zerolinecolor="rgb(255, 255, 255)",
                    showbackground=True,
                    backgroundcolor="rgb(230, 230,230)",
                ),
                yaxis=dict(
                    gridcolor="rgb(255, 255, 255)",
                    zerolinecolor="rgb(255, 255, 255)",
                    showbackground=True,
                    backgroundcolor="rgb(230, 230,230)",
                ),
                zaxis=dict(
                    gridcolor="rgb(255, 255, 255)",
                    zerolinecolor="rgb(255, 255, 255)",
                    showbackground=True,
                    backgroundcolor="rgb(230, 230,230)",
                ),
            ),
            showlegend=False,
        )
        return lines, layout

    def trisurf_3d(self):
        # x, y, z are one array
        points2D = numpy.vstack([self.x, self.y]).T
        tri = Delaunay(points2D)
        simplices = tri.simplices

        colormap, scale = colors.convert_colors_to_same_type(
            self.colorscale or None, colortype="tuple", return_default_colors=True
        )

        trisurf = figure_factory._trisurf.trisurf(
            x=self.x,
            y=self.y,
            z=self.z,
            simplices=simplices,
            show_colorbar=True,
            plot_edges=True,
            colormap=colormap,
            edges_color=self.color,
            scale=scale,
        )
        return trisurf, None

    def plot(self, method):
        """
        method: a method in this class to use [trisurf_3d, surface_3d, wireframe_3d]
        """
        if self.check_data():
            func = getattr(self, method)
            data, layout = func()
            kwargs = dict(data=data)

            if isinstance(data, dict):
                kwargs["layout"] = layout

            fig = graph_objects.Figure(**kwargs)
            fig.update_traces(self.get_traces())
            fig.update_layout(self.get_layout())

            self.write_plot(fig)
            return fig

__ppp = PRMP_Plotly_Plotter3D('file.html')


def plot(x, y, z, method='surface_3d', show=1):
        """
        x, y, z
        method: a method in this class to use [trisurf_3d, surface_3d, wireframe_3d]
            surface_3d: x, y are one array, while z is a grid
            wireframe_3d: x, y, z are grids
            trisurf_3d: x, y, z are one arrays
        """

        __ppp.fit_data(x, y, z)
        fig = __ppp.plot(method)

        if show:
            fig.show()



