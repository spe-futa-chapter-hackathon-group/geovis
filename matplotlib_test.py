from geovis import Dataset
from matplot_lib import plot


data = Dataset('complete_data.csv')

x, y, z = data.get_grids('idw')

plot(x, y, z)
