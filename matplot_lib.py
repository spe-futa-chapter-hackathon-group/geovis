import matplotlib.pyplot as plt
from coord import *


def plot(x, y, z, title='Surface Plot'):
    '''
    x, y, z are all grids
    '''

    fig = plt.figure()
    ax = plt.axes(projection='3d')
    ax.set_title(title)
    ax.plot_surface(x, y, z, cmap='viridis', edgecolor='none')
    plt.show()
