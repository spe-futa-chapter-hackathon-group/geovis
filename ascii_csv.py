import csv

def ascii_csv(infile, outfile):
    
#infile = r'eg.txt'
#outfile = r'eg.csv'


    text = csv.reader(open(infile, 'r'), delimiter = "\t")
    new_csv = csv.writer(open(outfile, 'w'))

    return new_csv.writerows(text)

