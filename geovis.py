from typing import Tuple, Union
import utm, numpy, pandas
from scipy.spatial import Delaunay
from scipy.optimize import curve_fit
from sympy import symbols, integrate


DATA_POINTS = Union[pandas.DataFrame, "Dataset"]
_flat_volume = {}


def to_geo(northing, easting, zone_number, zone_letter) -> tuple:
    latitude, longitude = utm.to_latlon(easting, northing, zone_number, zone_letter)
    return (latitude, longitude)


def to_utm(latitude, longitude) -> tuple:
    northing, easting, zone_number, zone_letter = utm.from_latlon(latitude, longitude)
    return (northing, easting, zone_number, zone_letter)

class Interpolation_Gridding:
    def __init__(self) -> None:
        self.x: numpy.ndarray = None
        self.y: numpy.ndarray = None
        self.z: numpy.ndarray = None
        self.z_grid: numpy.ndsarray = None
        self.z_flatten: numpy.ndsarray = None

        self.grid_size: int = 0
        self.exponent: int = 2
        self.distance: int = 10
        self.x_range = []
        self.y_range = []

        self.x_grid: numpy.ndarray = None
        self.y_grid: numpy.ndarray = None
        self.x_flatten: numpy.ndarray = None
        self.y_flatten: numpy.ndarray = None

    def fit_data(self, x, y, z, grid_size=100, linspace=1, exponent=2, distance=10):
        assert len(x) == len(y) == len(z)
        self.x = numpy.asarray(x)
        self.y = numpy.asarray(y)
        self.z = numpy.asarray(z)

        self.distance = distance
        self.grid_size = grid_size
        self.exponent = exponent
        self.x_range = []
        self.y_range = []

        self.calculate_ranges(linspace)

        self.x_grid, self.y_grid = numpy.meshgrid(self.x_range, self.y_range)
        self.x_flatten, self.y_flatten = self.x_grid.flatten(), self.y_grid.flatten()

    def _distance(self, x1, y1, x2, y2):
        sqrt = numpy.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)
        return sqrt

    def calculate_ranges(self, linspace):
        self.x_min, self.x_max = self.x.min(), self.x.max()
        self.y_min, self.y_max = self.y.min(), self.y.max()

        width = self.x_max - self.x_min
        length = self.y_max - self.y_min
        self.x_interval = width / self.grid_size
        self.y_interval = length / self.grid_size

        if linspace:
            self.x_range = numpy.linspace(self.x_min, self.x_max, self.grid_size)
            self.y_range = numpy.linspace(self.y_min, self.y_max, self.grid_size)
        else:

            self.x_range = numpy.arange(self.x_min, self.x_max, self.x_interval)
            self.y_range = numpy.arange(self.y_min, self.y_max, self.y_interval)

    def distance_matrix(self):
        obs = numpy.vstack((self.x, self.y)).T
        interp = numpy.vstack((self.x_flatten, self.y_flatten)).T
        d0 = numpy.subtract.outer(obs[:, 0], interp[:, 0])
        d1 = numpy.subtract.outer(obs[:, 1], interp[:, 1])
        return numpy.hypot(d0, d1)

    def get_grids(self, function, **kwargs):
        '''
        call fit_data before calling Interpolation_Gridding.get_grids()
        :param: function: [slow_idw, simple_rbf_linear, idw, rbf_multiquadric, rbf_inverse, rbf_gaussian, rbf_linear, rbf_cubic, rbf_quintic, rbf_thin_plate, kriging, random, trend]
        :param: kwargs: the parameters that the function we accept
        '''
        function = function.lower()

        func = getattr(self, function, None)
        assert func, f"{function} is not implemented"

        self.z_flatten = func(**kwargs)

        if check_data(self.z_flatten):
            self.z_grid = self.z_flatten.reshape((self.grid_size, self.grid_size))

        return self.x_range, self.y_range, self.z_grid

    def get_blocks(self, xz, yz):
        radius = self.grid_size  # block radius iteration distance
        nf = 0
        loop = 0
        while (
            nf <= self.distance
        ):  # will stop when numpy reaching at least nearest_points
            loop += 1
            x_block = []
            y_block = []
            z_block = []
            radius += self.grid_size  # add 10 unit each iteration
            xr_min, xr_max = xz - radius, xz + radius
            yr_min, yr_max = yz - radius, yz + radius

            for i in range(len(self.x)):
                # condition to test if a point is within the block
                xx, yy = self.x[i], self.y[i]
                if (xx >= xr_min and xx <= xr_max) and (yy >= yr_min and yy <= yr_max):
                    x_block.append(xx)
                    y_block.append(yy)
                    z_block.append(self.z[i])
            nf += len(x_block)  # calculate number of point in the block
            return x_block, y_block, z_block

    def idw_npoint(self, xz, yz):
        x_block, y_block, z_block = self.get_blocks(xz, yz)

        # calculate weight based on distance and
        w_list = []
        for j in range(len(x_block)):
            distance = self._distance(xz, yz, x_block[j], y_block[j])
            if distance > 0:
                weight = 1 / (distance ** self.exponent)
            else:
                weight = 0  # if meet this condition, it means distance <= 0, weight is set to 0
            w_list.append(weight)

        # check if there is 0 in weight list
        zero_check = 0 in w_list
        if zero_check == True:
            idx = w_list.index(0)  # find index for weight=0
            z_idw = z_block[idx]  # set the value to the current sample value
        else:
            wt = numpy.transpose(w_list)
            z_idw = numpy.dot(z_block, wt) / sum(
                w_list
            )  # idw calculation using dot product

        return z_idw

    def slow_idw(self, **kwargs):
        
        '''
        :param: kwargs: *unused
        '''

        z_grid = []
        for i in range(self.grid_size):
            xz = self.x_min + self.x_interval * i
            yz = self.y_min + self.y_interval * i
            # self.y_range.append(yz)
            # self.x_range.append(xz)
            z_range = []

            for j in range(self.grid_size):
                xz = self.x_min + self.x_interval * j
                z_idw = self.idw_npoint(xz, yz)
                z_range.append(z_idw)
            z_grid.append(z_range)

        z_grid = numpy.array(z_grid).flatten()
        return z_grid

    def idw(self, **kwargs):
        '''
        :param: kwargs: *unused
        '''
        distance_matrix = self.distance_matrix()

        # We have to check if there are any points in the distance_matrix that equals 0,
        # then find the indices of those points and equate them to 1,
        # to avoid 1 divided by 0
        for index, value in enumerate(self.z):
            points = distance_matrix[index]
            distance_matrix[index][points <= 0] = value

        # In IDW, weights are in reciprocals i.e 1 / distance
        weights = 1.0 / (distance_matrix ** self.exponent)

        # we then set the points in the distance_matrix that equals zero back to zero, after the reciprocal
        # weights[less_equals_zero_indices] = 0

        # Make weights sum to one
        weights /= weights.sum(axis=0)

        # Multiply the weights for each interpolated point by all observed Z-values
        z_flatten = numpy.dot(weights.T, self.z)

        return z_flatten

    def simple_rbf_linear(self, **kwargs):
        
        '''
        :param: kwargs: *unused
        '''
        distance_matrix = self.distance_matrix()

        # Mutual pariwise distances between observations
        internal_dist = distance_matrix(self.x, self.y, self.x, self.y)

        # Now solve for the weights such that mistfit at the observations is minimized
        weights = numpy.linalg.solve(internal_dist, self.z)

        # Multiply the weights for each interpolated point by the distances
        zi = numpy.dot(distance_matrix.T, weights)
        return zi

    def __scipy_rbf(self, function, smooth=0, epsilon=None, **kwargs):
        from scipy.interpolate import Rbf

        interp = Rbf(
            self.x, self.y, self.z, function=function, smooth=smooth, epsilon=epsilon
        )
        return interp(self.x_flatten, self.y_flatten)

    def rbf_multiquadric(self, **kwargs):
        return self.__scipy_rbf("multiquadric", **kwargs)

    def rbf_inverse(self, **kwargs):
        return self.__scipy_rbf("inverse", **kwargs)

    def rbf_gaussian(self, **kwargs):
        return self.__scipy_rbf("gaussian", **kwargs)

    def rbf_linear(self, **kwargs):
        return self.__scipy_rbf("linear", **kwargs)

    def rbf_cubic(self, **kwargs):
        return self.__scipy_rbf("cubic", **kwargs)

    def rbf_quintic(self, **kwargs):
        return self.__scipy_rbf("quintic", **kwargs)

    def rbf_thin_plate(self, **kwargs):
        return self.__scipy_rbf("thin_plate", **kwargs)

    def random(self, **kwargs):
        zmin = self.z.min()
        zmax = self.z.max()
        random_grid = numpy.random.uniform(
            low=zmin, high=zmax, size=(self.grid_size, self.grid_size)
        )
        return random_grid

    def trend(self, exponent=0, **kwargs):
        func = self._create_polynomial(exponent or self.exponent)
        fitted = numpy.vstack((self.x, self.y)).T
        popt, _ = curve_fit(func, (fitted[:, 0], fitted[:, 1]), self.z)

        return func((self.x_grid, self.y_grid), *popt)

    def _create_polynomial(self, order):
        if order is None:  # custom function by the user
            return None

        elif order == 0:

            def func(X, a):
                return a

        elif order == 1:

            def func(X, a, b, c):
                x1, x2 = X
                return a + b * x1 + c * x2

        elif order == 2:

            def func(X, a, b, c, d, e, f):
                x1, x2 = X
                return a + b * x1 + c * x2 + d * (x1 ** 2) + e * (x1 ** 2) + f * x1 * x2

        else:
            ...

        return func



class Coord:
    def __init__(self, x: float, y: float, z: float):
        self.x = x
        self.y = y
        self.z = z
    
    def __str__(self):
        return "point: {0}, {1}, {2}".format(self.x, self.y, self.z)

    def __sub__(self, other):
        """
        Returns the difference between 2 points in a cartesian plane.
        It is used to create a vector out of 2 points in 3D space.
        """
        minuend = numpy.array([self.x, self.y, self.z])
        subtrahend = numpy.array([other.x, other.y, other.z])
        return minuend - subtrahend

    def __lt__(self, other):
        """Comparison override used to sort points in function of their x
        coordinate. This was done so because of the order the double integral
        is applied when calculating the volume casted by an ABC traingle
        (defined by a set of 3 Cartesian Coordinates)
        """
        return self.x < other.x

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y and self.z == other.z



class Line2D:
    """A 2-Dimensional line"""

    def __init__(self, point_A: Coord, point_B: Coord):
        """Constructor

        Arguments:
        point_A: Cartesian Coordinate for point A
        point_B: Cartesian Coordinate for point B
        """
        self.point_A = point_A
        self.point_B = point_B

    def get_line_equation(self):
        """Returns a callable f(x): the line equation that connects point_A to
        point_B
        """
        if self.point_B.x - self.point_A.x == 0:  # line parallel to the y axis
            return None
        else:
            slope = (self.point_B.y - self.point_A.y) / (
                self.point_B.x - self.point_A.x
            )
            linear_constant = -slope * self.point_A.x + self.point_A.y
            x = symbols("x")
            return slope * x + linear_constant


class Triangle:
    """A triangle in a 3D Cartesian Coordinates System"""

    def __init__(
        self,
        point_A: Coord,
        point_B: Coord,
        point_C: Coord,
    ):
        self.point_A = point_A
        self.point_B = point_B
        self.point_C = point_C

    def get_plane_equation(self):
        """
        Returns the plane equation constants for the plane that contains points
        A, B and C.
        Plane equation: a*(x-xo) + b*(y-yo) + c*(z-zo) = 0
        """
        vector_AB = self.point_B - self.point_A
        vector_BC = self.point_C - self.point_B
        normal_vector = numpy.cross(vector_AB, vector_BC)
        a = normal_vector[0]
        b = normal_vector[1]
        c = normal_vector[2]
        xo = self.point_A.x
        yo = self.point_A.y
        zo = self.point_A.z
        x, y = symbols("x y")  # z = f(x, y)
        return ((-a * (x - xo) - b * (y - yo)) / c) + zo

    def get_volume(self):
        """
        Returns the volume from the polyhedron generated by triangle ABC and
        the XY plane
        """
        plane = self.get_plane_equation()
        # Define how to compute a double integral
        def compute_double_integral(
            outer_boundary_from, outer_boundary_to, line_from_equation, line_to_equation
        ):
            if (line_from_equation is None) or (line_to_equation is None):
                return 0.0  # vertical line
            x, y = symbols("x y")
            volume = integrate(
                plane,
                (y, line_from_equation, line_to_equation),
                (x, outer_boundary_from, outer_boundary_to),
            )
            return volume

        # Instantiate lines. Points are sorted on the x coordinate.
        points = [self.point_A, self.point_B, self.point_C]
        points.sort()
        sorted_point_A, sorted_point_B, sorted_point_C = points
        lineAB = Line2D(sorted_point_A, sorted_point_B)
        lineBC = Line2D(sorted_point_B, sorted_point_C)
        lineAC = Line2D(sorted_point_A, sorted_point_C)

        # Compute double integral 1:
        volume1 = compute_double_integral(
            sorted_point_A.x,
            sorted_point_B.x,
            lineAC.get_line_equation(),
            lineAB.get_line_equation(),
        )
        # Compute double integral 2:
        volume2 = compute_double_integral(
            sorted_point_B.x,
            sorted_point_C.x,
            lineAC.get_line_equation(),
            lineBC.get_line_equation(),
        )

        # Sum and return
        total_volume = abs(volume1) + abs(volume2)
        return total_volume


def print_progress(
    iteration, total, prefix="", suffix="", decimals=1, length=100, fill="█"
):
    """Terminal progress bar
    @params:
    iteration   - Required  : current iteration (Int)
    total       - Required  : total iterations (Int)
    prefix      - Optional  : prefix string (Str)
    suffix      - Optional  : suffix string (Str)
    decimals    - Optional  : positive number of decimals in percent
                              complete (Int)
    length      - Optional  : character length of bar (Int)
    fill        - Optional  : bar fill character (Str)
    """
    if not total:
        return
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + "-" * (length - filledLength)
    print("\r%s |%s| %s%% %s" % (prefix, bar, percent, suffix), end="\r")
    # Print New Line on Complete
    if iteration == total:
        print()  # last print


def get_volume(
    data_points: DATA_POINTS, x_column="x", y_column="y", z_column="z", log=False
)->Union[float, int]:
    data = Delaunay(
        data_points[[x_column, y_column]]
    ).simplices  # I think I can remove this. Do it after done with the corresponding cut/fill unit tests.
    mesh_volume = 0
    iteration = 0
    data_amount = len(data)
    for i in range(data_amount):
        A = data_points.iloc[data[i][0]]
        B = data_points.iloc[data[i][1]]
        C = data_points.iloc[data[i][2]]
        point_A = Coord(A[x_column], A[y_column], A[z_column])
        point_B = Coord(B[x_column], B[y_column], B[z_column])
        point_C = Coord(C[x_column], C[y_column], C[z_column])
        triangle = Triangle(point_A, point_B, point_C)
        volume = triangle.get_volume()
        mesh_volume += volume

        # update progress bar
        iteration += 1
        if log:
            print_progress(iteration, data_amount)
    return mesh_volume


def add_to_flat_volume(data: DATA_POINTS, ref_level, z_column="z"):
    """
    Sets the internal attribute for flat_volume.
    """
    data_flat = data.copy(deep=True)
    data_flat[z_column] = ref_level
    _flat_volume[ref_level] = get_volume(data_flat, log=False)


def get_cut_volume(data: DATA_POINTS, ref_level, log=True, z_column="z")->numpy.int64:
    """
    Returns the terrain cut volume, corresponding to the amount of volume
    required to be removed from terrain down to the ref_level

    :param ref_level: the reference level to be used. This is relative to
    the lowest point available in z.
    """
    data_cut = data.copy(deep=True)
    data_cut.loc[data_cut[z_column] < ref_level, z_column] = ref_level
    if ref_level not in _flat_volume.keys():
        add_to_flat_volume(ref_level, z_column=z_column)
    flat_volume = _flat_volume[ref_level]
    full_cut = get_volume(data_cut, log=log)
    return numpy.int64(full_cut - flat_volume)


def get_fill_volume(data: DATA_POINTS, ref_level, log=True, z_column="z")->numpy.int64:
    """
    Returns the terrain fill volume, corresponding to the amount of volume
    required to fill the terrain up to the ref_level

    :param ref_level: the reference level to be used. This is relative to
    the lowest point available in z.
    """
    if ref_level == 0.0:
        return 0.0  # quick exit when ref is 0.0.

    data_fill = data.copy(deep=True)
    data_fill.loc[data_fill[z_column] >= ref_level, z_column] = ref_level
    if ref_level not in _flat_volume.keys():
        add_to_flat_volume(ref_level, z_column=z_column)
    flat_volume = _flat_volume[ref_level]
    full_fill = get_volume(data_fill, log=log)
    return numpy.int64(flat_volume - full_fill)


def check_data(data) -> bool:
    try:
        if data == None:
            return False
        else:
            return True
    except:
        return True


class Dataset:
    def __init__(self, file="", data=None, x_column='x',y_column='y',z_column='z'):
        assert file or check_data(data)

        self.x_column = x_column
        self.y_column = y_column
        self.z_column = z_column

        self.intp_grid = Interpolation_Gridding()

        self.data: pandas.DataFrame = pandas.read_csv(file) if file else data

    @property
    def columns(self):
        return self.data.columns

    @property
    def volume(self):
        return get_volume(self.data)

    def __getitem__(self, item):
        return self.data[item]

    def __setitem__(self, item, value):
        self.data[item] = value

    def copy(self, *args, **kwargs):
        return self.data.copy(*args, **kwargs)
    
    def get_grids(self, function='simple_rbf_linear', gridKwargs={}, allgrids=True, **fitKwargs)->Tuple[numpy.array, numpy.array, numpy.array]:
        self.intp_grid.fit_data(self.data[self.x_column], self.data[self.y_column], self.data[self.z_column], **fitKwargs)

        values = self.intp_grid.get_grids(function, **gridKwargs)
        if allgrids:
            values = self.intp_grid.x_grid, self.intp_grid.y_grid, values[-1]
        return values
