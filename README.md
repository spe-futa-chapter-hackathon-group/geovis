## about geovis

geovis is a python package mainly for  Terrain (3D) Visualization of geospatial data sets gotten from topographic or hydrographic surveys. This is a very important aspect of the fundamental stage of any operation in sectors such as mining, energy, construction, oil and gas etc. that requires the planning, design and erection of one or more stuctures. Some of the direct applications includes exploration of solid minerals in mines (lithium/spodumene mining, limestone deposits, e.t.c), development of terrains for oil rig positioning both onshore and offshore, monitoring of progress of land reclamation (dredging operation) when creating artificial islands etc.

geovis.py package will be of great significance in analysis, depicting and easy interpretation of coordinates position in terms of generic properties as compared to other geospatial dataset libraries like geopandas, PostGIS rendering in 2d. geovis.py accepts transformed survey data (e.g. in CSV format) or raw survey data (e.g. in ASCII format) and can possibly transform raw data to a usable format. The package would also be able to process these geospatial data sets into digital terrain models (DTMs) for terrain visualization. Another function of the package would be the usability for volume computations from the geospatial data sets 


## ascii to csv file format
File format conversion function is also augmented for data in ascii format while returning a .csv file to same directory.


## 3d terrain visuals

geovis.py uses either plotly or matplotlib to produce 3d visualisation.

![a77f69a3-c4eb-4082-a36c-0b837307b121](/uploads/c0322fd36306956c2b576efc5eb8318e/a77f69a3-c4eb-4082-a36c-0b837307b121.jpg)


## Usage

![2ef464f8-6429-41f4-b875-0f4388f106c2](/uploads/49d21c96c1eb8c177a0c5c46e593d92e/2ef464f8-6429-41f4-b875-0f4388f106c2.jpg)



![fe72b84c-3b76-403e-a30e-e28127e6053c](/uploads/95900737fdd6ce94550bc31a83c6e0d3/fe72b84c-3b76-403e-a30e-e28127e6053c.jpg)



![ca7d25bf-e29a-4011-b9ba-d038e2731a73](/uploads/f763823561cea20772906d18660da7a4/ca7d25bf-e29a-4011-b9ba-d038e2731a73.jpg)

## Acknowledgment

We acknowledge SPE Lagos for the opportunity to be part of the 2022 hackathon competition.


