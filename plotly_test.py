from geovis import Dataset
from plot_ly import plot



data = Dataset('complete_data.csv')

x, y, z = data.get_grids('idw')

print(x.size, y.size, z.size)

plot(x, y, z)
